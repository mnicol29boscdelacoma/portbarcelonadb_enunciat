/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.boscdelacoma.portbarcelona.views.ui.javafx;

import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;
import cat.boscdelacoma.portbarcelona.model.business.entities.Vaixell;
import cat.boscdelacoma.portbarcelona.views.ui.test.PortBarcelona;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author marc
 */
public class PortFXMLController implements Initializable {

    @FXML
    private Label lblData;
    @FXML
    private Button btnExec;
    @FXML
    private TextArea txtConsole;
    @FXML
    private Label lblMaxim;
    @FXML
    private Label lblMinim;
    @FXML
    private Label lblMitja;
    
    private Vaixell vaixell;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LocalDate data = LocalDate.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        lblData.setText(String.format("DATA: %s\n", data.format(format)));
        //btnExec.setOnAction(e->System.out.println("hola"));

    }

    public PortFXMLController() {
        vaixell = new Vaixell();

        PortBarcelona.carregarVaixell(vaixell);
    }

    @FXML
    private void btnExecClick(ActionEvent event) {
        mostrarEstadistica(vaixell);
        mostrarContenidors(vaixell);
        descarregarVaixell(vaixell);
    }

    void descarregarVaixell(Vaixell vaixell) {

        txtConsole.appendText("\nDESCARREGAR VAIXELL\n");
        txtConsole.appendText("-------------------\n");

        txtConsole.appendText("CONTENIDOR         VOLUM\n");
        txtConsole.appendText("------------------------\n");
        Contenidor contenidor;

        for (int i = 0; i < vaixell.getnContenidors(); i++) {
            contenidor = vaixell.getContenidor(i);
            txtConsole.appendText(String.format("%s %12.2f m3\n", contenidor.getNumSerie(), contenidor.getVolum()));
        }
        txtConsole.appendText(String.format("\nVOLUM TOTAL: %8.2f m3\n", vaixell.getVolum()));

        vaixell.descarregar();
    }

    void mostrarContenidors(Vaixell vaixell) {
        Contenidor contenidor;
        int countMercaderies = 0;

        txtConsole.appendText("\nMOSTRAR CONTENIDORS VAIXELL\n");
        txtConsole.appendText("---------------------------\n");

        for (int i = 0; i < vaixell.getnContenidors(); i++) {
            contenidor = vaixell.getContenidor(i);
            txtConsole.appendText(String.format("%s\n", contenidor.getNumSerie()));
            countMercaderies += contenidor.getnMercaderies();
        }
        txtConsole.appendText(String.format("\nTOTAL MERCADERIES: %d\n", countMercaderies));
    }

    void mostrarEstadistica(Vaixell vaixell) {
        lblMaxim.setText(String.format("%.2f",vaixell.getMaxVolum()));
        lblMinim.setText(String.format("%.2f", vaixell.getMinVolum()));
        lblMitja.setText(String.format("%.2f", vaixell.getMitjaVolum()));
    }
}
