package cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.jdbc.mysql;

import cat.boscdelacoma.portbarcelona.model.persistence.exceptions.DAOException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


/**
 * Classe que implementa el patró Singleton per obtenir la connexió a la BD.
 *
 * @author Usuario
 */
public class MySQLConnection {

    private final String FILE_CONFIG = "resources/configMySQLproperties";

    private static MySQLConnection instance;
    private Connection connection;

    private MySQLConnection() throws DAOException {

        Properties prop = new Properties();


        try (InputStream inputStream = new FileInputStream(FILE_CONFIG)) {
            if (inputStream != null) {
                prop.load(inputStream);
                String url = prop.getProperty("url");
                String driver = prop.getProperty("driver").replaceAll("\"", "");
                String user = prop.getProperty("user");
                String password = prop.getProperty("password");

                try {
                    // TODO: obrir la connexió amb la BD.
                    
                } catch (ClassNotFoundException | SQLException ex) {
                    throw new DAOException("Error en establir connexió amb la base de dades." + ex);
                }
            } else {
                throw new DAOException("No es pot llegir la configuració de connexió amb la base de dades. Fitxer '" + FILE_CONFIG + "' no s'ha trobat");
            }
        } catch (IOException ex) {

        }

    }

    public Connection getConnection() {
        return connection;
    }

    public static MySQLConnection getInstance() throws DAOException {
        try {
            if (instance == null) {
                instance = new MySQLConnection();
            } else if (instance.getConnection().isClosed()) {
                instance = new MySQLConnection();
            }

            return instance;
        } catch (SQLException ex) {
            throw new DAOException("Error en accedir a la base de dades: " + ex);
        }
    }

}
