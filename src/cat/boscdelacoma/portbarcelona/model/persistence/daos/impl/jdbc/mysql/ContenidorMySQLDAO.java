package cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.jdbc.mysql;

import cat.boscdelacoma.portbarcelona.model.business.entities.Cisterna;
import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;
import cat.boscdelacoma.portbarcelona.model.business.entities.DryVan;
import cat.boscdelacoma.portbarcelona.model.business.entities.Refrigerat;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.contracts.ContenidorDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.exceptions.DAOException;
import cat.boscdelacoma.portbarcelona.model.persistence.utilities.JDBCUtils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Usuario
 */
public class ContenidorMySQLDAO implements ContenidorDAO {

    @Override
    public Contenidor getContenidorByNumSerie(String numSerie) throws DAOException {
        // l'objecte a retornar
        Contenidor contenidor = null;

        try (
                PreparedStatement query = MySQLConnection.getInstance().
                        getConnection().
                        prepareStatement("SELECT serialnumber, capacity, type, minTemp, color, estat "
                                + "FROM container "
                                + "WHERE serialNumber = ?")) {
                    // passem el paràmetre
                    query.setString(1, numSerie);
                    try (ResultSet reader = query.executeQuery();) {
                        if (reader.next()) {
                            contenidor = JDBCUtils.getContenidor(reader);
                        }
                    } catch (Exception ex) {
                        throw new DAOException("Error en accedir a la taula de Contenidors " + ex);
                    }

                } catch (SQLException ex) {
                    throw new DAOException("Error en accedir a la taula de Contenidors");
                }
                return contenidor;
    }

    @Override
    public Map<String, Contenidor> getContenidors() throws DAOException {
        Map<String, Contenidor> map = new TreeMap<>();
        Contenidor contenidor = null;
        try (
                PreparedStatement query = MySQLConnection.getInstance().
                        getConnection().
                        prepareStatement("SELECT serialnumber, capacity, type, minTemp, color, estat "
                                + "FROM container")) {

                    try (ResultSet reader = query.executeQuery();) {

                        while (reader.next()) {
                            contenidor = JDBCUtils.getContenidor(reader);
                            map.put(contenidor.getNumSerie(), contenidor);
                        }
                    } catch (Exception ex) {
                        // Lloc on fer el Rollback en cas de ser necessari
                        throw new DAOException("Error en accedir a la taula de Contenidors " + ex);

                    }

                } catch (SQLException ex) {
                    throw new DAOException("Error en accedir a la taula de Contenidors " + ex);
                }
                return map;
    }

    @Override
    public void saveContenidors(Map<String, Contenidor> contenidors) throws DAOException {
        try (
                PreparedStatement query = MySQLConnection.getInstance().
                        getConnection().
                        prepareStatement("INSERT INTO container "
                                + "VALUES(?, ?, ?, ?, ?, ?")) {

                    try {
                        MySQLConnection.getInstance().getConnection().setAutoCommit(false);

                        for (Map.Entry<String, Contenidor> contenidor : contenidors.entrySet()) {
                            query.setString(1, contenidor.getKey());
                            query.setFloat(2, contenidor.getValue().getVolum());
                            if (contenidor.getValue() instanceof DryVan) {
                                query.setString(3, "DryVan");
                                query.setString(5, ((DryVan) contenidor.getValue()).getColor());
                            } else if (contenidor.getValue() instanceof Refrigerat) {
                                query.setString(3, "Refrigerat");
                                query.setFloat(4, ((Refrigerat) contenidor.getValue()).getMinTemp());
                            } else if (contenidor.getValue() instanceof Cisterna) {
                                query.setString(3, "Cisterna");
                            }
                            query.setBoolean(6, contenidor.getValue().isObert());
                            query.executeUpdate();
                        }
                        MySQLConnection.getInstance().getConnection().commit();

                    } catch (Exception ex) {
                        // Lloc on fer el Rollback en cas de ser necessari
                        MySQLConnection.getInstance().getConnection().rollback();
                        throw new DAOException("Error en accedir a la taula de Contenidors");
                    } finally {
                        MySQLConnection.getInstance().getConnection().setAutoCommit(true);

                    }

                } catch (SQLException ex) {
                    throw new DAOException("Error en accedir a la taula de Contenidors");
                }
    }

}
