package cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.jdbc.mysql;

import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;
import cat.boscdelacoma.portbarcelona.model.business.entities.Mercaderia;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.contracts.MercaderiaDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.exceptions.DAOException;
import cat.boscdelacoma.portbarcelona.model.persistence.utilities.JDBCUtils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MercaderiaMySQLDAO implements MercaderiaDAO {

    @Override
    public List<Mercaderia> getMercaderies() throws DAOException {
        List<Mercaderia> list = new ArrayList<>();

        // TODO: obtenir totes les mercaderies que hi ha a la BD MySQL
        
        return list;
    }

    @Override
    public List<Mercaderia> getMercaderiesFromContainer(Contenidor container) throws DAOException {
        List<Mercaderia> list = new ArrayList<>();

        // TODO: obtenir totes les mercaderies de la BD  MySQL que estan dins el contenidor passat per paràmetre.
        return list;
    }

    @Override
    public void saveMercaderies(List<Mercaderia> mercaderies) throws DAOException {

        // TODO: desar totes les mercaderies de la List, dins la BD MySQL
    }

}
