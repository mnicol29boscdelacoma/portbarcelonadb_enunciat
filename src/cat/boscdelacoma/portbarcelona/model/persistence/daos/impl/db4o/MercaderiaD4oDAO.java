package cat.boscdelacoma.portbarcelona.model.persistence.daos.impl.db4o;

import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;
import cat.boscdelacoma.portbarcelona.model.business.entities.Mercaderia;
import cat.boscdelacoma.portbarcelona.model.persistence.daos.contracts.MercaderiaDAO;
import cat.boscdelacoma.portbarcelona.model.persistence.exceptions.DAOException;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Marc Nicolau
 */
public class MercaderiaD4oDAO implements MercaderiaDAO {

    @Override
    public List<Mercaderia> getMercaderies() throws DAOException {
        List<Mercaderia> list = new ArrayList<>();
        ObjectSet<Mercaderia> results = DB4OConnection.getInstance().getConnection().queryByExample(Mercaderia.class);
        for (Mercaderia mercaderia : results) {
            list.add(mercaderia);

        }

        return list;
    }

    @Override
    public List<Mercaderia> getMercaderiesFromContainer(Contenidor container) throws DAOException {
        List<Mercaderia> list = new ArrayList<>();

        ObjectSet<Mercaderia> results = DB4OConnection.getInstance().getConnection().query(new Predicate<Mercaderia>() {
            @Override
            public boolean match(Mercaderia mercaderia) {
                return mercaderia.getContenidor().getNumSerie().equalsIgnoreCase(container.getNumSerie());
            }
        });

        for (Mercaderia mercaderia : results) {
            list.add(mercaderia);
        }

        return list;
    }

    @Override
    public void saveMercaderies(List<Mercaderia> mercaderies) throws DAOException {
        for (Mercaderia mercaderia : mercaderies) {
            DB4OConnection.getInstance().getConnection().store(mercaderia);
        }

    }

}
