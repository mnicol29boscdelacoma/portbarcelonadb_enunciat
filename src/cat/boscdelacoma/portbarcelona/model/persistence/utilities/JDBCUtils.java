package cat.boscdelacoma.portbarcelona.model.persistence.utilities;

import cat.boscdelacoma.portbarcelona.model.business.entities.Cisterna;
import cat.boscdelacoma.portbarcelona.model.business.entities.Contenidor;
import cat.boscdelacoma.portbarcelona.model.business.entities.DryVan;
import cat.boscdelacoma.portbarcelona.model.business.entities.Mercaderia;
import cat.boscdelacoma.portbarcelona.model.business.entities.Refrigerat;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Usuario
 */
public class JDBCUtils {

    public static Contenidor getContenidor(ResultSet reader) throws SQLException {
        Contenidor contenidorBase = new Contenidor(reader.getString("serialnumber"), reader.getFloat("capacity"), reader.getBoolean("estat"));
        Contenidor contenidor = null;
        
        switch (reader.getString("type")) {
            case "DryVan":
                contenidor = new DryVan(contenidorBase, reader.getString("color"));
                break;
            case "Refrigerat":
                contenidor = new Refrigerat(contenidorBase, reader.getFloat("minTemp"));
                break;
            case "Cisterna":
                contenidor = new Cisterna(contenidorBase);
        }
        return contenidor;
    }

    public static Mercaderia getMercaderia(ResultSet reader) throws SQLException {
        Contenidor contenidor = new Contenidor();
        contenidor.setNumSerie(reader.getString("numseriecontainer"));

        Mercaderia mercaderia = new Mercaderia(reader.getString("description"), reader.getFloat("volume"), contenidor);
        mercaderia.setId(reader.getLong("id"));
        
        return mercaderia;
    }
}
