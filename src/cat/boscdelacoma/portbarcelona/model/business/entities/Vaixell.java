package cat.boscdelacoma.portbarcelona.model.business.entities;

import java.util.ArrayList;
import java.util.List;

public class Vaixell {

    public static final int MAX_CONTENIDORS = 1000;

    private List<Contenidor> contenidors;
    private float volumOcupat;

    public Vaixell() {
        this.contenidors = new ArrayList<>();
        this.volumOcupat = 0;
    }

    public Vaixell(float volumOcupat) {
        this();
        this.volumOcupat = volumOcupat;
    }

    /**
     * Afegeix un contenidor en el vaixell.
     *
     * Pot generar excepcions en cas que no es compleixin les restriccions
     * indicades.
     *
     * @param contenidor el contenidor que es vol afegir.
     */
    public void addContenidor(Contenidor contenidor) throws UnsupportedOperationException {

        if (contenidors.size() == MAX_CONTENIDORS) {
            // el contenidor està ple
            throw new UnsupportedOperationException("No es pot carregar el contenidor perquè el vaixell està ple");
        } else if (!contenidor.isValid()) {
            // el contenidor no es pot carregar => inspecció DUANA
            throw new UnsupportedOperationException("No es pot carregar el contenidor perquè no ha passat la inspecció de la DUANA");
        } else if (contenidor.getnMercaderies() == 0) {
            // el contenidor no té mercaderies
            throw new UnsupportedOperationException("No es pot carregar el contenidor perquè no conté mercaderies " + contenidor.getNumSerie());
        } else if (contenidor.isObert()) {
            // el contenidor està  obert
            throw new UnsupportedOperationException("No es pot carregar el contenidor perquè està obert");
        } else {
            this.contenidors.add( contenidor);

            // acumular el volum del contenidor
            this.volumOcupat += contenidor.getVolum();
        }
    }

    /**
     * Obtenir el volum ocupat per les mercaderies dels contenidors del vaixell
     *
     * @return obtenir el volum ocupat per les mercaderies dels contenidors del
     * vaixell
     */
    public float getVolum() {
        return volumOcupat;
    }

    /**
     * Obetnir el contenidor indicat.
     *
     * @param position la posició del contenidor que es vol obtenir
     * @return el contenidor a que s'ha demanat.
     */
    public Contenidor getContenidor(int pos) {
        return contenidors.get(pos);
    }

    public int getnContenidors() {
        return contenidors.size();
    }

    public List<Contenidor> getContenidors() {
        return this.contenidors;
    }

    public float getMinVolum() {
        return (float)contenidors.stream().mapToDouble(x->x.getVolum()).min().getAsDouble();
    }
    public float getMaxVolum() {
        return (float)contenidors.stream().mapToDouble(x->x.getVolum()).max().getAsDouble();
    }
    public float getMitjaVolum() {
        return (float)contenidors.stream().mapToDouble(x->x.getVolum()).average().getAsDouble();
    }
    /**
     * Descarrega el vaixell
     */
    public void descarregar() {
        contenidors.clear();
        this.volumOcupat = 0;
    }

}
